package com.plandiv.imed.presenter;

import android.util.Log;

import com.plandiv.imed.api.ApiClient;
import com.plandiv.imed.api.ApiInterface;
import com.plandiv.imed.contract.ReportView;
import com.plandiv.imed.model.Agency;
import com.plandiv.imed.model.Indicator;
import com.plandiv.imed.model.RequestTargetReport;
import com.plandiv.imed.model.RequestTrendingReport;
import com.plandiv.imed.model.TargetReportResponse;
import com.plandiv.imed.model.TrendingModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportPresenter extends BasePresenter {
    private WeakReference<ReportView> view;
    private ApiInterface apiInterface;

    public ReportPresenter(ReportView reportView) {
        view = new WeakReference<>(reportView);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }
    public ArrayList<Agency> getAgencyList(){
        return loginResponse.getAgencies();
    }
    public ArrayList<Indicator> getIndicatorByAgencyId(int agencyId){
        for(Agency agency : loginResponse.getAgencies()){
            if(agency.getId() == agencyId){
                return agency.getIndicators();
            }
        }
        return null;
    }

    /**
     * month should be month number-year like: 08-2019
     * @param indicatorId
     * @param agencyId
     * @param month
     */

    public void getTargetAchievement(int indicatorId, int agencyId, String month, String location){
        RequestTargetReport requestTargetReport = new RequestTargetReport();
        requestTargetReport.setIndicatorId(indicatorId);
        requestTargetReport.setAgencyId(agencyId);
        requestTargetReport.setMonth(month);
        requestTargetReport.setIndicatorLocation(location);
        getView().showProgressBar();
        apiInterface.getTargetAchievementReport(requestTargetReport).enqueue(new Callback<ArrayList<TargetReportResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<TargetReportResponse>> call, Response<ArrayList<TargetReportResponse>> response) {
                getView().hideProgressBar();
                if(response.body()!=null){
                    ArrayList<TargetReportResponse> targetReportResponses = response.body();
                    if(targetReportResponses.size()>0){
                        getView().showTargetAchievement(targetReportResponses.get(0));
                    }


                }
            }

            @Override
            public void onFailure(Call<ArrayList<TargetReportResponse>> call, Throwable t) {
                getView().hideProgressBar();
                getView().showFailedMessage(t.getMessage());
            }
        });

    }

    /**
     * fromDate and todate like "2019-01-01" this formate
     * @param indicatorId
     * @param agencyId
     * @param fromDate
     * @param toDate
     */
    public void getTrending(int indicatorId, int agencyId, String fromDate, String toDate, String location){
        RequestTrendingReport requestTrendingReport = new RequestTrendingReport();
        requestTrendingReport.setIndicatorId(indicatorId);
        requestTrendingReport.setAgencyId(agencyId);
        requestTrendingReport.setFromDate(fromDate);
        requestTrendingReport.setToDate(toDate);
        requestTrendingReport.setIndicatorLocation(location);
        Log.v("REPORT_VIEW","getTrending>>"+requestTrendingReport);
        getView().showProgressBar();
        apiInterface.getTrendingReport(requestTrendingReport).enqueue(new Callback<ArrayList<TrendingModel>>() {
            @Override
            public void onResponse(Call<ArrayList<TrendingModel>> call, Response<ArrayList<TrendingModel>> response) {
                getView().hideProgressBar();
                if(response.body()!=null){
                    getView().showTrend(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TrendingModel>> call, Throwable t) {
                getView().hideProgressBar();
                getView().showFailedMessage(t.getMessage());
            }
        });
    }


    @Override
    ReportView getView() {
        if (view != null) {
            return view.get();
        } else {
            return null;
        }
    }

    @Override
    void onDestroy() {
        view = null;
    }

    @Override
    boolean isLoggedIn() {
        return isLoggedIn;
    }

    @Override
    void onLoggedOut() {

    }
}
