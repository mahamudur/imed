package com.plandiv.imed.presenter;


import com.plandiv.imed.Utils.Logger;
import com.plandiv.imed.Utils.PostLogin;
import com.plandiv.imed.Utils.SharedPreference;
import com.plandiv.imed.api.ApiClient;
import com.plandiv.imed.api.ApiInterface;
import com.plandiv.imed.contract.LoginView;
import com.plandiv.imed.model.LoginResponse;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter extends BasePresenter {
    private WeakReference<LoginView> view;
    private ApiInterface apiInterface;

    public LoginPresenter(LoginView loginView) {
        view = new WeakReference<>(loginView);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public String getUserName() {
        return SharedPreference.getStringValue(getView().getContext(), SharedPreference.KEY_USER_NAME);
    }

    public String getPassword() {
        return SharedPreference.getStringValue(getView().getContext(), SharedPreference.KEY_PASSWORD);
    }

    private void storeUserNamePassword(String userName, String password) {
        SharedPreference.setStringValue(getView().getContext(), SharedPreference.KEY_USER_NAME, userName);
        SharedPreference.setStringValue(getView().getContext(), SharedPreference.KEY_PASSWORD, password);
    }

    public void postLogin(boolean isRemember, String userName, String password) {
        getView().showProgressBar();
        if (isRemember) {
            storeUserNamePassword(userName, password);
        } else {
            storeUserNamePassword("", "");
        }

        apiInterface.postLogin(userName,userName,password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                getView().hideProgressBar();
                if (response.body() != null) {
                    LoginResponse loginResponse = response.body();
                    updateLoginResponse(loginResponse);
                    getView().startMainScreen();
                } else {
                    getView().showFailedMessage("Failed to login now, please try again.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                getView().hideProgressBar();
                Logger.debugLog("Login failed", ">>" + t.getMessage());
                getView().showFailedMessage("Failed to login now, please try again.");
            }
        });
    }

    @Override
    LoginView getView() {
        if (view != null) {
            return view.get();
        } else {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    @Override
    void onLoggedOut() {

    }
}
