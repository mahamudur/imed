package com.plandiv.imed.presenter;



import com.plandiv.imed.Utils.CacheManager;
import com.plandiv.imed.contract.View;
import com.plandiv.imed.model.LoginResponse;

abstract class BasePresenter {
    abstract View getView();

    abstract void onDestroy();

    abstract boolean isLoggedIn();

    abstract void onLoggedOut();

    protected LoginResponse loginResponse;
    protected CacheManager cacheManager;
    protected boolean isLoggedIn;

    public BasePresenter() {
        cacheManager = CacheManager.getInstance();
        loginResponse = getLoginResponse();
        isLoggedIn = loginResponse != null;
        if (!isLoggedIn) {
            onLoggedOut();
        }
    }

    public void updateLoginResponse(LoginResponse loginResponse) {
        cacheManager.cacheLoginResponse(loginResponse);
    }

    public LoginResponse getLoginResponse() {
        return cacheManager.getLoginResponse();
    }

    public void clearLoginCache() {
        cacheManager.clearLoginCache();
    }
}
