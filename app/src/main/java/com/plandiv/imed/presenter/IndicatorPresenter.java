package com.plandiv.imed.presenter;

import com.plandiv.imed.Utils.FormFrequencyType;
import com.plandiv.imed.Utils.PostLogin;
import com.plandiv.imed.api.ApiClient;
import com.plandiv.imed.api.ApiInterface;
import com.plandiv.imed.contract.IndicatorView;
import com.plandiv.imed.contract.View;
import com.plandiv.imed.model.IndicatorForm;
import com.plandiv.imed.model.SubmitData;
import com.plandiv.imed.model.SubmitIndicatorForm;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IndicatorPresenter extends BasePresenter {
    private WeakReference<IndicatorView> view;
    private ApiInterface apiInterface;
    private ArrayList<IndicatorForm> indicatorForms = new ArrayList<>();

    public ArrayList<IndicatorForm> getAllIndicatorForms() {
        return indicatorForms;
    }

    public IndicatorPresenter(IndicatorView indicatorView){
        view = new WeakReference<>(indicatorView);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }
    public void fetchAllIndicator(){
        getView().showProgressBar();
        PostLogin postLogin = new PostLogin(loginResponse.getUserName());
//        PostLogin postLogin = new PostLogin("plan_admin");
        apiInterface.getIndicatorFormList(postLogin).enqueue(new Callback<ArrayList<IndicatorForm>>() {
            @Override
            public void onResponse(Call<ArrayList<IndicatorForm>> call, Response<ArrayList<IndicatorForm>> response) {
                getView().hideProgressBar();
                if(response.body()!=null){
                    indicatorForms.addAll(response.body());
                    getView().showIndicatorForm();
                }else{
                    getView().showFailedMessage("Response not found");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<IndicatorForm>> call, Throwable t) {
                getView().hideProgressBar();
                getView().showFailedMessage(t.getMessage());

            }
        });
    }
    public void submitData(ArrayList<IndicatorForm> indicatorForms){
        SubmitData submitData = new SubmitData();
        SubmitIndicatorForm submitIndicatorForm = new SubmitIndicatorForm();
        submitIndicatorForm.setIndicatorForms(indicatorForms);
        submitIndicatorForm.setUserName(loginResponse.getUserName());
        submitData.setSubmitIndicatorForm(submitIndicatorForm);
        apiInterface.submitFormData(submitData).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getView().hideProgressBar();
                if(response.body()!=null && response.body().equalsIgnoreCase("true")){
                    getView().showFailedMessage("Form submitted");
                    getView().finishView();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getView().hideProgressBar();
                getView().showFailedMessage(t.getMessage());
            }
        });
    }
    public  ArrayList<IndicatorForm> getFrequencyWiseList(String frequency){
        if(frequency.equalsIgnoreCase(FormFrequencyType.YEARLY)) return indicatorForms;

        ArrayList<IndicatorForm> frequencyWiseList = new ArrayList<>();
        for(IndicatorForm indicatorForm : indicatorForms){
            if(frequency.equalsIgnoreCase(FormFrequencyType.QUARTERLY)){
                if(indicatorForm.getReportingFrequency().equalsIgnoreCase(FormFrequencyType.QUARTERLY)
                 || indicatorForm.getReportingFrequency().equalsIgnoreCase(FormFrequencyType.MONTHLY)){
                    frequencyWiseList.add(indicatorForm);
                }
            }else if(frequency.equalsIgnoreCase(FormFrequencyType.MONTHLY)){
                if(indicatorForm.getReportingFrequency().equalsIgnoreCase(FormFrequencyType.MONTHLY)){
                    frequencyWiseList.add(indicatorForm);
                }
            }

        }
        return frequencyWiseList;
    }

    @Override
    IndicatorView getView() {
        if (view != null) {
            return view.get();
        } else {
            return null;
        }
    }

    @Override
    void onDestroy() {
        view = null;

    }

    @Override
    boolean isLoggedIn() {
        return isLoggedIn;
    }

    @Override
    void onLoggedOut() {

    }
}
