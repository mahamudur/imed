package com.plandiv.imed.Utils;

import android.util.Log;

import com.plandiv.imed.BuildConfig;

/**
 * Created by mahmud
 */

public class Logger {
    public  static void debugLog(String tag,String message){
        if(BuildConfig.DEBUG){
            Log.d(tag,message);
        }
    }
}
