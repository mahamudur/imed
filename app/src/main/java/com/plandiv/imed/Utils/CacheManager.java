package com.plandiv.imed.Utils;

import android.content.Context;
import android.widget.Toast;


import com.plandiv.imed.MyApplication;
import com.plandiv.imed.model.LoginResponse;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CacheManager {
    private static final String KEY_LOGIN = "key_login";

    private CacheManager() {
    }

    private static CacheManager instance = null;

    public static CacheManager getInstance() {
        if (instance == null) {
            instance = new CacheManager();
        }
        return instance;
    }

    private void writeObject(String key, Object object) throws IOException {
        FileOutputStream fos = MyApplication.getContext().openFileOutput(key, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    private Object readObject(String key) throws IOException, ClassNotFoundException {
        FileInputStream fis = MyApplication.getContext().openFileInput(key);
        ObjectInputStream ois = new ObjectInputStream(fis);
        return ois.readObject();
    }

    public void cacheLoginResponse(LoginResponse loginResponse) {
        try {
            writeObject(KEY_LOGIN, loginResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LoginResponse getLoginResponse() {
        LoginResponse loginResponse = null;
        try {
            loginResponse = (LoginResponse) readObject(KEY_LOGIN);
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
        }

        return loginResponse;
    }

    public String getUserName() {
        LoginResponse loginResponse = getLoginResponse();
        if (loginResponse != null) return loginResponse.getUserName();
        else                       {
            return "";
        }
    }
    public String getAgencyName() {
        LoginResponse loginResponse = getLoginResponse();
        if (loginResponse != null) return loginResponse.getAgency();
        else                       {
            return "";
        }
    }

    public void clearLoginCache() {
        try {
            writeObject(KEY_LOGIN, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
