package com.plandiv.imed.Utils;

public class FormFrequencyType {
    public static final String YEARLY = "Yearly";
    public static final String MONTHLY = "Monthly";
    public static final String QUARTERLY = "Quarterly";

    public static String getFrequency(int month){
        if(month%12 == 0) return FormFrequencyType.YEARLY;
        if(month % 3 == 0) return FormFrequencyType.QUARTERLY;
        return FormFrequencyType.MONTHLY;
    }
}
