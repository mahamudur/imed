package com.plandiv.imed.Utils;

import com.google.gson.annotations.SerializedName;

public class PostLogin {
    @SerializedName("username")
    String username;

    public PostLogin(String username) {
        this.username = username;
    }
}
