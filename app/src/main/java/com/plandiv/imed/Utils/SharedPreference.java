package com.plandiv.imed.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.plandiv.imed.MyApplication;

import java.util.Collections;
import java.util.List;

public class SharedPreference {
    public static final String PREFS_NAME = "imed_app";
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_PASSWORD = "password";

    public static boolean getBooleanValue(final Context context, String key) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }


    public static void setBooleanValue(final Context context, String key,
                                       Boolean status) {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(key, status);
        editor.apply();
    }


    public static String getStringValue(final Context context, String key) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .getString(key, "");
    }

    public static void setStringValue(final Context context, String key,
                                      String value) {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        editor.putString(key, value);
        editor.apply();
    }

    public static int getIntValue(final Context context, String key,int setDefault) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .getInt(key, setDefault);
    }

    public static void setIntValue(final Context context, String key,
                                   int value) {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(key, value);
        editor.apply();
    }

    public static long getLongValue(final Context context, String key) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getLong(key, 0);
    }

    public static void setLongValue(final Context context, String key,
                                    long value) {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }
    public static void saveSharedPreferencesLogList(Context context, String key,  List<String> callLog) {
        SharedPreferences mPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public static List<String> getList(final Context context, String key) {
        return Collections.singletonList(context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
                .getString(key, ""));
    }
    public static void deletePref(){
        SharedPreferences prefs = MyApplication.getContext().getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=prefs.edit();
        editor.clear();
        editor.commit();

    }

}
