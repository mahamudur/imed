package com.plandiv.imed.Utils;

public class FormResponseType {
    public static final int INTEGER = 1;
    public static final int TEXT = 2;
}
