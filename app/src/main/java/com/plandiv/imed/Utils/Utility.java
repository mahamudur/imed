package com.plandiv.imed.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ViewTarget;
import com.plandiv.imed.MyApplication;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class Utility {
    public static final String WEBSITE_URL = "https://www.xtragift.com/";
    public static final String PRIVACY_POLICY_LINK = "https://www.xtragift.com/en/privacy-policy";

    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String yyyy_mm_dd = "yyyy-mm-dd";
    public static final String dateWithComma = "EEEE, MMMM dd, yyyy hh:mm a";

    public class PUT_EXTRA {
        public static final String EXTRA_BASE_ORDER = "extra_base";
        public static final String EXTRA_BASE_FRAGMENT = "base_fragment";
        public static final String EXTRA_NAME = "extra_name";
        public static final String EXTRA_MESSAGE = "extra_name";
    }

    public static void registeredPushBroadcast(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver,
                new IntentFilter(PUSH_NOTIFICATION));
    }

    public static void notifyPushBroadcast(Context context, String message) {
        Intent intent = new Intent();
        intent.putExtra(PUT_EXTRA.EXTRA_MESSAGE, message);
        intent.setAction(PUSH_NOTIFICATION);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void unregisterPushBroadcast(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static void setImageAsTarget(final String url, final ImageView iv, final int defaultImg) {
        ViewTarget viewTarget = new ViewTarget<ImageView, GlideDrawable>(iv) {
            @Override
            public void onLoadStarted(Drawable placeholder) {
                if (defaultImg != 0) iv.setImageResource(defaultImg);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                if (defaultImg != 0) iv.setImageResource(defaultImg);
            }

            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                iv.setImageDrawable(resource.getCurrent());
            }
        };
        Glide.with(MyApplication.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(viewTarget);
    }

    public static String convertTimezoneToDateString(String timeWithTimeZone, String dateFormat) {
        if (TextUtils.isEmpty(timeWithTimeZone)) return "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");//2018-10-28T00:00:00.000
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = sdf.parse(timeWithTimeZone);
            SimpleDateFormat df = new SimpleDateFormat(dateFormat);
            df.setTimeZone(TimeZone.getDefault());
            return df.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        try {
            DateFormat formatter = new SimpleDateFormat(dateFormat);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            String date = formatter.format(calendar.getTime());
            return date;
        } catch (Exception e) {
            // TODO: handle exception
            return "";
        }
    }

    public static void showCustomToast(Context context, String message, int length, int verticalGravity) {
        Toast toast = Toast.makeText(context, message, length);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        toast.setGravity(verticalGravity | Gravity.CENTER_HORIZONTAL, 0, 0);
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(18);
        toast.show();
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager conMgr = null;
        boolean connected = false;

        try {
            conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            connected = (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected());
            return connected;
        } catch (Exception e) {
            // Do nothing, since failed cases will be handled outside of this scope.
        }
        return connected;
    }
}
