package com.plandiv.imed.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.plandiv.imed.model.Agency;
import com.plandiv.imed.model.Indicator;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<Object> {

    private Context context;
    private ArrayList<Object> values;

    public SpinnerAdapter(Context context, int textViewResourceId,
                          ArrayList<Object> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public Object getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        Object object = values.get(position);
        if(object instanceof Agency){
            label.setText(((Agency) object).getUnitName());
        }else if(object instanceof Indicator){
            label.setText(((Indicator) object).getIndicatorName());
        }


        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        Object object = values.get(position);
        if(object instanceof Agency){
            label.setText(((Agency) object).getUnitName());
        }else if(object instanceof Indicator){
            label.setText(((Indicator) object).getIndicatorName());
        }

        return label;
    }
}
