package com.plandiv.imed.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.plandiv.imed.R;
import com.plandiv.imed.contract.ReportView;
import com.plandiv.imed.model.Agency;
import com.plandiv.imed.model.Indicator;
import com.plandiv.imed.presenter.ReportPresenter;
import com.plandiv.imed.view.adapter.SpinnerAdapter;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public abstract class BaseReportFragment extends Fragment implements ReportView {
    protected String locationName;
    int agencyId;
    int indicatorId;
    @BindView(R.id.location_spinner)
    Spinner locationSpinner;
    @BindView(R.id.agency_spinner)
    Spinner agencySpinner;
    @BindView(R.id.indicator_spinner)
    Spinner indicatorSpinner;
    @BindView(R.id.month_txt)
    TextView textViewMonth;
    @BindView(R.id.progress_bar)
    protected ProgressBar progressBar;
    protected ReportPresenter reportPresenter;

    abstract void onAfterViewCreated(View view);
    private SimpleDateFormat df = new SimpleDateFormat("MMM-yyyy");
    Calendar c = Calendar.getInstance();;
    String formattedDate;
    Activity activity;
    protected int lastDay[] = {31,28,31, 30,31,30,31,31,30,31,30,31};

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity)context;
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report,container,false);

        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public final void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateDatePicker();
        reportPresenter = new ReportPresenter(this);
        agencySpinner.setAdapter(new SpinnerAdapter(getActivity(),android.R.layout.simple_spinner_item,new ArrayList<>(reportPresenter.getAgencyList())));
        onAfterViewCreated(view);
    }
    @OnItemSelected({R.id.location_spinner,R.id.agency_spinner,R.id.indicator_spinner})
    public void spinnerItemSelected(Spinner spinner, int position) {
        switch (spinner.getId()){
            case R.id.location_spinner:
                locationName = locationSpinner.getSelectedItem().toString();
                break;
            case R.id.agency_spinner:
                agencyId = ((Agency)agencySpinner.getSelectedItem()).getId();
                if(reportPresenter.getIndicatorByAgencyId(agencyId)!=null){
                    indicatorSpinner.setAdapter(new SpinnerAdapter(getActivity(),android.R.layout.simple_spinner_item,new ArrayList<>(reportPresenter.getIndicatorByAgencyId(agencyId))));
                }
                break;
            case R.id.indicator_spinner:
                indicatorId = ((Indicator)indicatorSpinner.getSelectedItem()).getId();
                break;
        }
    }
    private void updateDatePicker(){

        formattedDate = df.format(c.getTime());

        textViewMonth.setText(formattedDate);
    }
    public String addZeroForMonth(String month){
        if(month.length()==1) return "0"+month;
        return month;
    }
    @OnClick({R.id.month_txt,R.id.submit_btn,R.id.left_btn,R.id.right_btn})
    void onClick(View v){
        switch (v.getId()){
            case R.id.submit_btn:
                filterTargetAchievement(c.get(Calendar.MONTH),c.get(Calendar.YEAR));
                break;
            case R.id.month_txt:
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(activity, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        c.set(Calendar.YEAR,selectedYear);
                        c.set(Calendar.MONTH,selectedMonth);
                        updateDatePicker();
                        filterTargetAchievement(selectedMonth,selectedYear);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH));

                builder.setActivatedMonth(c.get(Calendar.MONTH))
                        .setMinYear(1990)
                        .setActivatedYear(c.get(Calendar.YEAR))
                        .setMaxYear(c.get(Calendar.YEAR))
                        .setTitle("Select month")
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                            }
                        })
                        .build()
                        .show();
                break;
            case R.id.left_btn:
                c.add(Calendar.MONTH, -1);
                updateDatePicker();
                break;
            case R.id.right_btn:
                c.add(Calendar.MONTH, 1);
                updateDatePicker();
                break;

        }
    }
}
