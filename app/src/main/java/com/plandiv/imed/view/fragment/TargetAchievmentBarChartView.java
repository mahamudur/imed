package com.plandiv.imed.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.plandiv.imed.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TargetAchievmentBarChartView extends Fragment {

    @BindView(R.id.barchart)
    BarChart barChart;
    private int target,achievment,globalTrg,globalAchev;

    public static TargetAchievmentBarChartView getInstance(){
        TargetAchievmentBarChartView trendingBarChartView = new TargetAchievmentBarChartView();
        Bundle bundle = new Bundle();
        trendingBarChartView.setArguments(bundle);
        return trendingBarChartView;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_barchart,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    public void updateChart(int target, int achievment , int globalTrg, int globalAchv){
        this.target = target;
        this.achievment = achievment;
        this.globalAchev = globalAchv;
        this.globalTrg = globalTrg;
        drawChart();
    }
    private void drawChart() {
        BarData data = new BarData(getXAxisValues(),getDataSet());
        barChart.setData(data);
        barChart.animateXY(2000, 2000);
        barChart.setDescription("");
        barChart.invalidate();

    }
    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("");
        xAxis.add("");
//        xAxis.add("");
//        xAxis.add("");
        return xAxis;
    }
    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = new ArrayList<>();

        ArrayList<BarEntry> yLablsTar = new ArrayList<>();
        yLablsTar.add(new BarEntry(target,0));
        BarDataSet set1 = new BarDataSet(yLablsTar, "Target");
        set1.setColor(Color.GREEN);

        ArrayList<BarEntry> yLablsAch = new ArrayList<>();
        yLablsAch.add(new BarEntry(achievment,1));
        BarDataSet set2 = new BarDataSet(yLablsAch, "Achievement");
        set2.setColor(Color.BLUE);

        ArrayList<BarEntry> yLablsGTar = new ArrayList<>();
        yLablsGTar.add(new BarEntry(globalTrg,2));
        BarDataSet set3 = new BarDataSet(yLablsGTar, "Global Target");
        set3.setColor(Color.RED);

        ArrayList<BarEntry> yLablsGAch = new ArrayList<>();
        yLablsGAch.add(new BarEntry(globalAchev,3));
        BarDataSet set4 = new BarDataSet(yLablsGAch, "Global Achievement");
        set4.setColor(Color.YELLOW);

        dataSets.add(set1);
        dataSets.add(set2);
        dataSets.add(set3);
        dataSets.add(set4);

        return dataSets;
    }
}
