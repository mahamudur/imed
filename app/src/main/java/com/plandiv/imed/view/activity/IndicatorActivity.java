package com.plandiv.imed.view.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.plandiv.imed.R;
import com.plandiv.imed.Utils.FormFrequencyType;
import com.plandiv.imed.Utils.Utility;
import com.plandiv.imed.contract.IndicatorView;
import com.plandiv.imed.model.IndicatorForm;
import com.plandiv.imed.presenter.IndicatorPresenter;
import com.plandiv.imed.view.adapter.IndicatorAdapter;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class IndicatorActivity extends AppCompatActivity implements IndicatorView {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.month_txt)
    TextView textViewMonth;

    private IndicatorPresenter presenter;
    private IndicatorAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private SimpleDateFormat df = new SimpleDateFormat("MMM-yyyy");
    private SimpleDateFormat dfSubmitted = new SimpleDateFormat("yyyy-MM-dd");
    Calendar c = Calendar.getInstance();;
    String formattedDate;
    String submittedDate;
    String indicatorLocation;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indicators);
        ButterKnife.bind(this);

        updateDatePicker();
        presenter = new IndicatorPresenter(this);
        presenter.fetchAllIndicator();
    }
    private void updateDatePicker(){

        formattedDate = df.format(c.getTime());
        submittedDate = dfSubmitted.format(c.getTime());

        textViewMonth.setText(formattedDate);
    }
    @OnItemSelected(R.id.select_location_spinner)
    public void spinnerItemSelected(Spinner spinner, int position) {
        if(position>0){
            indicatorLocation = spinner.getSelectedItem().toString();
        }
    }
    @OnClick({R.id.back_btn,R.id.refresh_btn,R.id.search_btn,R.id.month_txt,R.id.submit_btn,R.id.left_btn,R.id.right_btn})
    void onClick(View v){
        switch (v.getId()){
            case R.id.back_btn:
                finish();
                break;
            case R.id.refresh_btn:
                break;
            case R.id.search_btn:
                break;
            case R.id.submit_btn:
                if(TextUtils.isEmpty(indicatorLocation)){
                    Toast.makeText(this,"Select indicator location",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(adapter!=null){
                    for(IndicatorForm indicatorForm : adapter.getIndicatorForms()){
                        indicatorForm.setIndicatorLocation(indicatorLocation);
                        indicatorForm.setSubmittedDate(submittedDate);
                    }
                    presenter.submitData(adapter.getIndicatorForms());
                }
                break;
            case R.id.month_txt:
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(IndicatorActivity.this, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        c.set(Calendar.YEAR,selectedYear);
                        c.set(Calendar.MONTH,selectedMonth);
                        updateDatePicker();
                        filterIndicatorByFrequency(selectedMonth);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH));

                builder.setActivatedMonth(c.get(Calendar.MONTH))
                        .setMinYear(1990)
                        .setActivatedYear(c.get(Calendar.YEAR))
                        .setMaxYear(c.get(Calendar.YEAR))
                        .setTitle("Select month")
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                            }
                        })
                        .build()
                        .show();
//                DatePickerDialog toDialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year, monthOfYear, dayOfMonth) -> {
//                    c.set(year,monthOfYear,dayOfMonth);
//                    updateDatePicker();
//                    filterIndicatorByFrequency(monthOfYear);
//                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
//                toDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                toDialog.show();
                break;
            case R.id.left_btn:
                c.add(Calendar.MONTH, -1);
                updateDatePicker();
                filterIndicatorByFrequency(c.get(Calendar.MONTH));
                break;
            case R.id.right_btn:
                c.add(Calendar.MONTH, 1);
                updateDatePicker();
                filterIndicatorByFrequency(c.get(Calendar.MONTH));
                break;

        }
    }

    @Override
    public void showIndicatorForm() {
        filterIndicatorByFrequency(c.get(Calendar.MONTH));

    }
    private void filterIndicatorByFrequency(int month){
        if(adapter == null){
            adapter = new IndicatorAdapter(this);
            adapter.addItem(presenter.getFrequencyWiseList(FormFrequencyType.getFrequency(month+1)));
            linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(adapter);
        }else{
            adapter.addItem(presenter.getFrequencyWiseList(FormFrequencyType.getFrequency(month+1)));
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void showFailedMessage(String message) {
        Utility.showCustomToast(this,message, Toast.LENGTH_SHORT, Gravity.CENTER_VERTICAL);

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showEmptyScreen() {

    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void loggedOut() {

    }
}
