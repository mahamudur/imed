package com.plandiv.imed.view.activity;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.plandiv.imed.R;
import com.plandiv.imed.Utils.Utility;
import com.plandiv.imed.contract.LoginView;
import com.plandiv.imed.presenter.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView {
    @BindView(R.id.edit_user_name)
    EditText editTextUserName;
    @BindView(R.id.password)
    EditText editTextPassword;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.remember_check)
    CheckBox checkBoxRememberMe;
    @BindView(R.id.password_img)
    ImageView ivShowPassword;

    private LoginPresenter presenter;
    private boolean isShowPasswordEnabled;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter(this);
        if (presenter.isLoggedIn()) {
            startMainScreen();
            return;
        }
        setContentView(R.layout.login_screen);
        ButterKnife.bind(this);
       // editTextUserName.setText("imed_admin");
       // editTextPassword.setText("12345678");
        populateRememberMe();
    }

    @OnClick({R.id.sign_in_btn, R.id.password_img})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_btn:
                if (isValid()) {
                    if (!Utility.isConnectedToInternet(this)) {
                        Utility.showCustomToast(this, getString(R.string.no_internet), Toast.LENGTH_SHORT, Gravity.CENTER);
                    } else {
                        postLogin();
                    }
                }
                break;
            case R.id.password_img:
                showHidePassword();
                break;
        }

    }

    private void showHidePassword() {
        isShowPasswordEnabled = !isShowPasswordEnabled;
        ivShowPassword.setImageDrawable(getResources().getDrawable(isShowPasswordEnabled ? R.drawable.ic_show_password_select : R.drawable.ic_show_password));
        editTextPassword.setTransformationMethod(isShowPasswordEnabled ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
    }

    private void populateRememberMe(){
        if(!TextUtils.isEmpty(presenter.getUserName())){
            editTextUserName.setText(presenter.getUserName());
            editTextPassword.setText(presenter.getPassword());
            checkBoxRememberMe.setChecked(true);
        }
    }

    private void postLogin() {
        presenter.postLogin(checkBoxRememberMe.isChecked(),editTextUserName.getText().toString(), editTextPassword.getText().toString());
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(editTextUserName.getText().toString())) {
            editTextUserName.setError("Empty user name");
            return false;
        }
        if (TextUtils.isEmpty(editTextPassword.getText().toString())) {
            editTextPassword.setError("Empty password");
            return false;
        }
        return true;
    }

    @Override
    public void startMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (getIntent().getExtras() != null) {
            intent.putExtras(getIntent().getExtras());
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void showFailedMessage(String message) {
        Utility.showCustomToast(this, message, Toast.LENGTH_SHORT, Gravity.CENTER);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void loggedOut() {

    }

    @Override
    public void showEmptyScreen() {

    }

    @Override
    public Context getContext() {
        return this;
    }

//    private void populateTermsAndPrivacy(TextView view) {
//        SpannableStringBuilder spanTxt = new SpannableStringBuilder("Privacy Policy");
//        spanTxt.setSpan(new ClickableSpan() {
//            @Override
//            public void onClick(View widget) {
//                openWebPage(Utility.PRIVACY_POLICY_LINK);
//            }
//        }, spanTxt.length() - "Privacy Policy".length(), spanTxt.length(), 0);
//        view.setMovementMethod(LinkMovementMethod.getInstance());
//        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
//    }

    public void openWebPage(String url) {
        Uri webUrl = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webUrl);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
