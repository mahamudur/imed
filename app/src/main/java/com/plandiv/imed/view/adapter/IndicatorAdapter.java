package com.plandiv.imed.view.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.plandiv.imed.R;
import com.plandiv.imed.Utils.FormResponseType;
import com.plandiv.imed.model.IndicatorForm;

import java.util.ArrayList;

public class IndicatorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<IndicatorForm> indicatorForms;
    private Context context;

    public IndicatorAdapter(Context context) {
        this.context = context;
        indicatorForms = new ArrayList<>();
    }

    public void addItem(ArrayList<IndicatorForm> indicatorForms) {
        this.indicatorForms.clear();
        this.indicatorForms.addAll(indicatorForms);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case FormResponseType.INTEGER:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_edittext_number, parent, false));
                case FormResponseType.TEXT:
                    return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_edittext, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IndicatorForm indicatorForm = indicatorForms.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.textViewIndicatorName.setText(indicatorForm.getIndicatorName());
        viewHolder.editTextIndicatorValue.setText(indicatorForm.getIndicatorValue()+"");
        viewHolder.editTextIndicatorValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                indicatorForm.setIndicatorValue(s.toString());

            }
        });
    }
    public boolean isNotEmpty(RecyclerView recyclerView){
        for(int i = 0; i< indicatorForms.size();i++){
            if(indicatorForms.get(i).getIndicatorValue().isEmpty()){
                ViewHolder viewHolder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
                viewHolder.editTextIndicatorValue.setError("Value can not empty");
                return false;
            }
        }
        return true;
    }
    public ArrayList<IndicatorForm> getIndicatorForms(){
        return indicatorForms;
    }

    @Override
    public int getItemViewType(int position) {
        return indicatorForms.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return indicatorForms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewIndicatorName;
        private EditText editTextIndicatorValue;
        private View myView;

        private ViewHolder(View view) {
            super(view);
            textViewIndicatorName = view.findViewById(R.id.indicator_name_txt);
            editTextIndicatorValue = view.findViewById(R.id.indicator_value);
            myView = view;
        }

        public View getView() {
            return myView;
        }
    }
}
