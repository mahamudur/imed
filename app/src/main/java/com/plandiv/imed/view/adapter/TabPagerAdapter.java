package com.plandiv.imed.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plandiv.imed.MyApplication;
import com.plandiv.imed.R;

import java.util.ArrayList;
import java.util.List;


public class TabPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();
        private final List<Integer> fragmentImageList = new ArrayList<>();

        public TabPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title, int tabImage) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
            fragmentImageList.add(tabImage);
        }
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }



    public View getTabView(int position) {
        View tab = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.custom_tab, null);
        ImageView image = tab.findViewById(R.id.image_view_tab);
        TextView textView = tab.findViewById(R.id.tab_title);
//        View dividerView = (View) tab.findViewById(R.id.divider);
//        if (position + 1 == getCount()) {
//            dividerView.setVisibility(View.GONE);
//        }
        image.setImageResource(fragmentImageList.get(position));
        textView.setText(fragmentTitleList.get(position));
        return tab;
    }
}
