package com.plandiv.imed.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plandiv.imed.view.fragment.TargetAndAchievementFragment;
import com.plandiv.imed.view.fragment.TrendingFragment;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 1) {
            return new TrendingFragment();
        } else if (position == 0){
            return new TargetAndAchievementFragment();
        }
        return null;
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 4;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 1:
                return "Trend";
            case 0:
                return "Target and Achievement";
            default:
                return null;
        }
    }

}
