package com.plandiv.imed.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.plandiv.imed.R;
import com.plandiv.imed.view.adapter.SimpleFragmentPagerAdapter;
import com.plandiv.imed.view.adapter.TabPagerAdapter;
import com.plandiv.imed.view.fragment.TargetAndAchievementFragment;
import com.plandiv.imed.view.fragment.TrendingFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportActivity extends AppCompatActivity {
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPagerFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        setViewPager();
    }
    private void setViewPager() {
        TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TargetAndAchievementFragment(), "Target and Achievement", 0);
        adapter.addFragment(new TrendingFragment(), "Trend", 0);
        mViewPagerFragment.setAdapter(adapter);
//        mViewPagerFragment.setOffscreenPageLimit(2);
        mViewPagerFragment.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabLayout.setupWithViewPager(mViewPagerFragment);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

    }
    @OnClick({R.id.back_btn})
    void onClick(View v){
        switch ( v.getId()){
            case R.id.back_btn:
                finish();
                break;
        }
    }

}
