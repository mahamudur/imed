package com.plandiv.imed.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.plandiv.imed.R;
import com.plandiv.imed.model.TrendingModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrendingBarChartView extends Fragment {

    @BindView(R.id.barchart)
    BarChart barChart;
    private ArrayList<TrendingModel> trendingModels;

    public static TrendingBarChartView getInstance(){
        TrendingBarChartView trendingBarChartView = new TrendingBarChartView();
        Bundle bundle = new Bundle();
        trendingBarChartView.setArguments(bundle);
        return trendingBarChartView;
    }
    public void updateData(ArrayList<TrendingModel> trendingModels){
        this.trendingModels = trendingModels;
        Log.v("REPORT_VIEW","trendingModels:"+trendingModels.size()+":"+trendingModels);
        drawChart();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_barchart,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    private void drawChart() {
        BarData data = new BarData(getXAxisValues(), getDataSet());
        barChart.setData(data);
        barChart.animateXY(2000, 2000);
        barChart.invalidate();
        Log.v("REPORT_VIEW","invalidate:");

    }
    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        for(int i= 0; i< trendingModels.size(); i++){
            xAxis.add(trendingModels.get(i).getMonth());
        }
        return xAxis;
    }
    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> yLabls = new ArrayList<>();
        for(int i= 0; i< trendingModels.size(); i++){
            yLabls.add(new BarEntry(trendingModels.get(i).getValue(),i));

            Log.v("REPORT_VIEW","trendingModels:"+trendingModels.size()+":"+trendingModels);
        }
        BarDataSet set1 = new BarDataSet(yLabls, "# of achievement");
        set1.setColor(Color.rgb(0, 155, 0));
        dataSets.add(set1);
        return dataSets;
    }
}
