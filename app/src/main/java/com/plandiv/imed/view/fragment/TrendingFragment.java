package com.plandiv.imed.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.plandiv.imed.R;
import com.plandiv.imed.model.TargetReportResponse;
import com.plandiv.imed.model.TrendingModel;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;


public class TrendingFragment extends BaseReportFragment {
    @BindView(R.id.barchart_trend)
    BarChart barChart;
    @BindView(R.id.from)
    TextView textViewFromDate;
    @BindView(R.id.to)
    TextView textViewToDate;
    private ArrayList<TrendingModel> trendingModels;
    private String fromDate, toDate, currentDate;
    private int month, year;
    @Override
    void onAfterViewCreated(View view) {
        view.findViewById(R.id.date_picker).setVisibility(View.GONE);
        view.findViewById(R.id.date_range).setVisibility(View.VISIBLE);
        view.findViewById(R.id.container).setVisibility(View.GONE);
        view.findViewById(R.id.barchart_trend).setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        currentDate = year + "-" + addZeroForMonth(month+"") ;

        toDate = currentDate+"-"+lastDay[month-1];
        fromDate = currentDate+"-01";
        updateFilter();
    }
    @OnClick({R.id.from, R.id.to})
    void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.to:
//                DatePickerDialog toDialog = new DatePickerDialog(activity, R.style.DialogTheme, (datePicker, year, monthOfYear, dayOfMonth) -> {
//                    int monthValue = monthOfYear+1;
//                    String filterToDate = year + "-" + monthValue + "-" + dayOfMonth;
//
//                    textViewToDate.setText(filterToDate.equals(currentDate) ? "Today" : filterToDate);
//                    toDate = filterToDate;
//                    updateFilter();
//                }, year, month, day);
//
//                toDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                toDialog.show();
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(activity, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                    int monthValue = selectedMonth+1;
                    String filterToDate = selectedYear + "-" + addZeroForMonth(monthValue+"");

                    textViewToDate.setText(filterToDate);
                    toDate = filterToDate+'-'+lastDay[selectedMonth];
                    updateFilter();
                    }
                }, year, month-1);

                builder.setActivatedMonth(month-1)
                        .setMinYear(1990)
                        .setActivatedYear(year)
                        .setMaxYear(year)
                        .setTitle("Select month")
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                            }
                        })
                        .build()
                        .show();
                break;
            case R.id.from:
//                DatePickerDialog fromDialog = new DatePickerDialog(activity, R.style.DialogTheme, (datePicker, year, monthOfYear, dayOfMonth) -> {
//                    int monthValue = monthOfYear+1;
//                    String filterFromDate = year + "-" + monthValue + "-" + dayOfMonth;
//
//                    textViewFromDate.setText(filterFromDate.equals(currentDate) ? "Today" : filterFromDate);
//                    fromDate = filterFromDate;
//                    updateFilter();
//                }, year, month, day);
//
//                fromDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                fromDialog.show();
                MonthPickerDialog.Builder builder2 = new MonthPickerDialog.Builder(activity, new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        int monthValue = selectedMonth+1;
                        String filterFromDate = selectedYear + "-" + addZeroForMonth(monthValue+"");

                        textViewFromDate.setText(filterFromDate);
                        fromDate = filterFromDate+"-01";
                        updateFilter();
                    }
                }, year, month-1);

                builder2.setActivatedMonth(month-1)
                        .setMinYear(1990)
                        .setActivatedYear(year)
                        .setMaxYear(year)
                        .setTitle("Select month")
                        .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                            @Override
                            public void onMonthChanged(int selectedMonth) {
                            }
                        })
                        .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                            @Override
                            public void onYearChanged(int selectedYear) {
                            }
                        })
                        .build()
                        .show();
                break;
        }
    }
    private void updateFilter() {
        if (TextUtils.isEmpty(textViewToDate.getText().toString())) {
            toDate = currentDate;
            textViewToDate.setText(toDate+"");
        }

        if (TextUtils.isEmpty(textViewFromDate.getText().toString())) {
            fromDate = currentDate;
            textViewFromDate.setText(fromDate+"");
        }

        if (!TextUtils.isEmpty(textViewFromDate.getText().toString()) && !TextUtils.isEmpty(textViewToDate.getText().toString())) {
            reportPresenter.getTrending(indicatorId,agencyId,fromDate,toDate,locationName);
        }
    }

    @Override
    public void filterTargetAchievement(int month, int year) {
        reportPresenter.getTrending(indicatorId,agencyId,fromDate,toDate,locationName);
    }

    @Override
    public void showTargetAchievement(TargetReportResponse targetReportResponse) {

    }

    @Override
    public void showTrend(ArrayList<TrendingModel> trendingModels) {
        updateData(trendingModels);

    }

    @Override
    public void showFailedMessage(String message) {

    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyScreen() {

    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    @Override
    public void loggedOut() {

    }
    public void updateData(ArrayList<TrendingModel> trendingModels){
        this.trendingModels = trendingModels;
        Log.v("REPORT_VIEW","trendingModels:"+trendingModels.size()+":"+trendingModels);
        drawChart();
    }
    private void drawChart() {
        BarData data = new BarData(getXAxisValues(), getDataSet());
        barChart.setData(data);
        barChart.setDescription("");
        barChart.animateXY(2000, 2000);
        barChart.invalidate();
        Log.v("REPORT_VIEW","invalidate:");

    }
    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        for(int i= 0; i< trendingModels.size(); i++){
            xAxis.add(trendingModels.get(i).getMonth());
        }
        return xAxis;
    }
    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        ArrayList<BarEntry> yLabls = new ArrayList<>();
        for(int i= 0; i< trendingModels.size(); i++){
            yLabls.add(new BarEntry(trendingModels.get(i).getValue(),i));
        }
        BarDataSet set1 = new BarDataSet(yLabls, "# of achievement");
        set1.setColor(Color.rgb(0, 155, 0));
        dataSets.add(set1);
        return dataSets;
    }
}
