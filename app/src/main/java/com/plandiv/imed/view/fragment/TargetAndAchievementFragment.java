package com.plandiv.imed.view.fragment;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.plandiv.imed.R;
import com.plandiv.imed.model.TargetReportResponse;
import com.plandiv.imed.model.TrendingModel;

import java.util.ArrayList;

public class TargetAndAchievementFragment extends BaseReportFragment {
    private TargetAchievmentBarChartView targetAchievmentBarChartView;


    @Override
    void onAfterViewCreated(View view) {
        view.findViewById(R.id.date_range).setVisibility(View.GONE);
        view.findViewById(R.id.date_picker).setVisibility(View.VISIBLE);
        view.findViewById(R.id.container).setVisibility(View.VISIBLE);
        view.findViewById(R.id.barchart_trend).setVisibility(View.GONE);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        targetAchievmentBarChartView = TargetAchievmentBarChartView.getInstance();
        transaction.replace(R.id.container, targetAchievmentBarChartView);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void filterTargetAchievement(int month, int year) {
        reportPresenter.getTargetAchievement(indicatorId,agencyId,(month+1)+"-"+year,locationName);

    }

    @Override
    public void showTargetAchievement(TargetReportResponse targetReportResponse) {
        targetAchievmentBarChartView.updateChart(targetReportResponse.getTarget(),targetReportResponse.getAchievement(),targetReportResponse.getGlobalTerget(),targetReportResponse.getTotalAchievement());
    }

    @Override
    public void showTrend(ArrayList<TrendingModel> trendingModels) {

    }

    @Override
    public void showFailedMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyScreen() {

    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    @Override
    public void loggedOut() {

    }
}
