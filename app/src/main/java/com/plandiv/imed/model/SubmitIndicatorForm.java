package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;
import com.plandiv.imed.Utils.FormResponseType;

import java.util.ArrayList;

public class SubmitIndicatorForm {
    @SerializedName("data_json")
    ArrayList<IndicatorForm> indicatorForms;
    @SerializedName("username")
    String userName;
    public ArrayList<IndicatorForm> getIndicatorForms() {
        return indicatorForms;
    }

    public void setIndicatorForms(ArrayList<IndicatorForm> indicatorForms) {
        this.indicatorForms = indicatorForms;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
