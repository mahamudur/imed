package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class LoginResponse implements Serializable {
    @SerializedName("username")
    String userName;
    @SerializedName("password")
    String password;
    @SerializedName("agency")
    String agency;
    @SerializedName("list_of_agency")
    ArrayList<Agency> agencies;

    public ArrayList<Agency> getAgencies() {
        return agencies;
    }

    public String getAgency() {
        return agency;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
