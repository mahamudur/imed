package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;
import com.plandiv.imed.Utils.FormResponseType;

public class IndicatorForm {
    @SerializedName("form_serial_no")
    int formSerialNo;
    @SerializedName("indicator_id")
    int indicatorId;
    @SerializedName("indicator_name")
    String indicatorName;
    @SerializedName("reporting_frequency")
    String reportingFrequency;
    @SerializedName("response_type")
    String responseType;
    @SerializedName("indicator_value")
    private String indicatorValue ="";
    @SerializedName("indicator_location")
    String indicatorLocation;
    @SerializedName("submitted_date")
    String submittedDate;

    public String getIndicatorLocation() {
        return indicatorLocation;
    }

    public void setIndicatorLocation(String indicatorLocation) {
        this.indicatorLocation = indicatorLocation;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(String submittedDate) {
        this.submittedDate = submittedDate;
    }
    private int viewType;

    public String getIndicatorValue() {
        return indicatorValue;
    }

    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }


    public int getViewType() {
        switch (responseType){
            case "Integer":
                viewType = FormResponseType.INTEGER;
                break;
            case "Text":
                viewType = FormResponseType.TEXT;
                break;
        }
        return viewType;
    }

    public int getFormSerialNo() {
        return formSerialNo;
    }

    public void setFormSerialNo(int formSerialNo) {
        this.formSerialNo = formSerialNo;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public String getReportingFrequency() {
        return reportingFrequency;
    }

    public void setReportingFrequency(String reportingFrequency) {
        this.reportingFrequency = reportingFrequency;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
