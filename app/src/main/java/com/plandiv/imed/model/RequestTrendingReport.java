package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

public class RequestTrendingReport {
    @SerializedName("indicator")
    private int indicatorId;
    @SerializedName("agency")
    private int agencyId;
    @SerializedName("from_date")
    private String fromDate;
    @SerializedName("to_date")
    private String toDate;
    @SerializedName("indicator_location")
    private String indicatorLocation;

    public String getIndicatorLocation() {
        return indicatorLocation;
    }

    public void setIndicatorLocation(String indicatorLocation) {
        this.indicatorLocation = indicatorLocation;
    }
    public int getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(int indicatorId) {
        this.indicatorId = indicatorId;
    }

    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public String toString() {
        return "RequestTrendingReport{" +
                "indicatorId=" + indicatorId +
                ", agencyId=" + agencyId +
                ", fromDate='" + fromDate + '\'' +
                ", toDate='" + toDate + '\'' +
                '}';
    }
}
