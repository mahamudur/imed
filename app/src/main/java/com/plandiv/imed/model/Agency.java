package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Agency implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("org_unit_name")
    private String unitName;
    @SerializedName("list_of_indicators")
    private ArrayList<Indicator> indicators;

    public int getId() {
        return id;
    }

    public String getUnitName() {
        return unitName;
    }

    public ArrayList<Indicator> getIndicators() {
        return indicators;
    }
}
