package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

public class RequestTargetReport {
    @SerializedName("indicator")
    private int indicatorId;
    @SerializedName("agency")
    private int agencyId;
    @SerializedName("rpt_month")
    private String month;

    @SerializedName("indicator_location")
    private String indicatorLocation;

    public String getIndicatorLocation() {
        return indicatorLocation;
    }

    public void setIndicatorLocation(String indicatorLocation) {
        this.indicatorLocation = indicatorLocation;
    }

    public int getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(int indicatorId) {
        this.indicatorId = indicatorId;
    }

    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
