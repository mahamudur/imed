package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

public class SubmitData {
    @SerializedName("data")
    SubmitIndicatorForm submitIndicatorForm;

    public SubmitIndicatorForm getSubmitIndicatorForm() {
        return submitIndicatorForm;
    }

    public void setSubmitIndicatorForm(SubmitIndicatorForm submitIndicatorForm) {
        this.submitIndicatorForm = submitIndicatorForm;
    }
}
