package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Indicator implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("reporting_org_unit")
    private String orgUnit;
    @SerializedName("indicator_name")
    private String indicatorName;


    public int getId() {
        return id;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public String getIndicatorName() {
        return indicatorName;
    }
}
