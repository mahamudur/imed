package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

public class TrendingModel {
    @SerializedName("month_name")
    private String month;
    @SerializedName("achievement")
    private int value;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TrendingModel{" +
                "month='" + month + '\'' +
                ", value=" + value +
                '}';
    }
}
