package com.plandiv.imed.model;

import com.google.gson.annotations.SerializedName;

public class TargetReportResponse {
    @SerializedName("indicator_id")
    private int indicatorId;
    @SerializedName("target")
    private int target;
    @SerializedName("achievement")
    private int  achievement;
    @SerializedName("global_trgt")
    private int globalTerget;
    @SerializedName("total_achievement")
    private int totalAchievement;

    public int getGlobalTerget() {
        return globalTerget;
    }

    public void setGlobalTerget(int globalTerget) {
        this.globalTerget = globalTerget;
    }

    public int getTotalAchievement() {
        return totalAchievement;
    }

    public void setTotalAchievement(int totalAchievement) {
        this.totalAchievement = totalAchievement;
    }

    public int getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(int indicatorId) {
        this.indicatorId = indicatorId;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getAchievement() {
        return achievement;
    }

    public void setAchievement(int achievement) {
        this.achievement = achievement;
    }
}
