package com.plandiv.imed;

import android.app.Application;
import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Created by mahmud
 */

public class MyApplication extends Application {
    private static WeakReference<Context> mContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static MyApplication getInstance(Context context) {
        return ((MyApplication) context.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = new WeakReference<>(getApplicationContext());
    }

    public static Context getContext() {
        return mContext.get();
    }
}
