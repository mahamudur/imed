package com.plandiv.imed.contract;

import android.content.Context;

public interface IndicatorView extends View {
    void showIndicatorForm();
    void showFailedMessage(String message);
    void finishView();
    Context getContext();
}
