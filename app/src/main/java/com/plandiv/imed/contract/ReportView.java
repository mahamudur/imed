package com.plandiv.imed.contract;

import android.content.Context;

import com.plandiv.imed.model.TargetReportResponse;
import com.plandiv.imed.model.TrendingModel;

import java.util.ArrayList;

public interface ReportView extends View {
    void filterTargetAchievement(int month, int year);
    void showTargetAchievement(TargetReportResponse targetReportResponse);
    void showTrend(ArrayList<TrendingModel> trendingModels);
    void showFailedMessage(String message);
    Context getContext();
}
