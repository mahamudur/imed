package com.plandiv.imed.contract;

import android.content.Context;

public interface View {
    void showProgressBar();
    void hideProgressBar();
    void showEmptyScreen();
    Context getActivityContext();
    void loggedOut();

}
