package com.plandiv.imed.contract;

import android.content.Context;

public interface LoginView extends View {
    void startMainScreen();
    void showFailedMessage(String message);
    Context getContext();
}
