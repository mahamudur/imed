package com.plandiv.imed.api;
import com.plandiv.imed.Utils.PostLogin;
import com.plandiv.imed.model.IndicatorForm;
import com.plandiv.imed.model.LoginResponse;
import com.plandiv.imed.model.RequestTargetReport;
import com.plandiv.imed.model.RequestTrendingReport;
import com.plandiv.imed.model.SubmitData;
import com.plandiv.imed.model.TargetReportResponse;
import com.plandiv.imed.model.TrendingModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    //http://xtra.dev.aplectrum.com/api/offer-avail-process/pending-avail-transactions/outlet-qr-code/{outletQrCode}
    @POST("imed/indicator_form_json")
    Call<ArrayList<IndicatorForm>> getIndicatorFormList(@Body PostLogin postLogin);
    @GET("/{userName}/login/")
    Call<LoginResponse> postLogin(@Path("userName") String userName,
                                 @Query("username") String username, @Query("password") String password);
    @POST("imed/indicator_form_submit")
    Call<String> submitFormData(@Body SubmitData submitData);

    @POST("imed/target_achievement_rpt")
    Call<ArrayList<TargetReportResponse>> getTargetAchievementReport(@Body RequestTargetReport requestTargetReport);

    @POST("imed/achievement_trend_rpt")
    Call<ArrayList<TrendingModel>> getTrendingReport(@Body RequestTrendingReport requestTrendingReport);
}
