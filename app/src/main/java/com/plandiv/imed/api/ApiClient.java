package com.plandiv.imed.api;

import android.os.Build;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.plandiv.imed.BuildConfig;
import com.plandiv.imed.MyApplication;
import com.plandiv.imed.R;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by mahmud
 */

public class ApiClient {
    private static Retrofit retrofit = null;

    private ApiClient() {
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            SSLContext sslContext = getCustomSSLContext();
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            if (sslContext != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                okHttpClientBuilder.sslSocketFactory(sslContext.getSocketFactory());
            }
            okHttpClientBuilder.addInterceptor(chain -> {
                        Request request = chain.request();
                        Request newRequest;

                        newRequest = request.newBuilder()
                                .build();
                        Response response = chain.proceed(newRequest);
                        return response;
                    });
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(okHttpClientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    private static SSLContext getCustomSSLContext() {
        InputStream caInput = null;
        SSLContext sslContext = null;
        try {
            caInput = new BufferedInputStream(MyApplication.getContext().getResources().openRawResource(R.raw.ca));
            Certificate ca = CertificateFactory.getInstance("X.509").generateCertificate(caInput);

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);

            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);

        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (caInput != null) {
                try {
                    caInput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sslContext;
    }
}
